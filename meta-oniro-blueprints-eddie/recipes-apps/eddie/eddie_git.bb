# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT

SUMMARY = "Eddie blueprint: enabling distributed intelligence in Oniro"
DESCRIPTION = "Enabling distributed intelligence in Oniro"
LICENSE = "Apache-2.0"

LIC_FILES_CHKSUM = "file://LICENSES/Apache-2.0.txt;md5=ef3dabb8f39493f4ea410bebc1d01755"

DEPENDS += "libcoap glib-2.0 jsoncpp"

SRC_URI += "git://gitlab.eclipse.org/eclipse/oniro-core/eddie.git;protocol=https;branch=main \
            file://eddie-server.service \
            file://org.eddie.TestInterface"

SRCREV = "1a666c9d2e01ec85bb7264bc3b40b6b0e91908ff"

S = "${WORKDIR}/git"

inherit cmake pkgconfig systemd

SYSTEMD_SERVICE:${PN} = "eddie-server.service"

FILES:${PN} += "${datadir}/**"

do_install:append() {
    install -d ${D}${systemd_unitdir}/system/
    install -m 0644 ${WORKDIR}/eddie-server.service ${D}${systemd_unitdir}/system/

    install -d ${D}${datadir}/dbus-1/system.d/
    install -m 0644 ${WORKDIR}/org.eddie.TestInterface ${D}${datadir}/dbus-1/system.d/
}

EXTRA_OECMAKE += "-DSKIP_TESTS=true"
